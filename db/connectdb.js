const mongoose = require("mongoose");
const uri = process.env.DATABASE_URL || "mongodb+srv://admin:P@ssw0rd@cluster0-3kwrt.mongodb.net/domoplus?retryWrites=true&w=majority";

mongoose.Promise = global.Promise;
mongoose.connect(uri, { useNewUrlParser: true });

module.exports = { mongoose }
