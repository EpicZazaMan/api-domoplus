const {mongoose} = require('../db/connectdb');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const store = new mongoose.Schema({
    ID_machine: {
        type: Number,
        default: 7
    },
    ouverture: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conso:{
        type: Number,
        required: false
    }
});


store.plugin(autoIncrement.plugin, 'store');
var Store = mongoose.model('store', store);

module.exports = { Store };