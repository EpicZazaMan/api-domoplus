const { mongoose } = require('../db/connectdb.js');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const domotique = new mongoose.Schema({
    nom: {
        type: String,
        required: true
    },
    icon: {
        type: String,
        required: true
        },
    etat: {
        type: Boolean,
        default:false,
    }
});


domotique.plugin(autoIncrement.plugin, 'Domotique');
var Domotique = mongoose.model('Domotique', domotique);

module.exports = { Domotique };
