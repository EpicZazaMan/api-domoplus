const {mongoose} = require('../db/connectdb');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const lavelinge = new mongoose.Schema({
    ID_machine: {
        type: Number,
        default: 6
    },
    lavage: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conso:{
        type: Number,
        required: false
    }
});


lavelinge.plugin(autoIncrement.plugin, 'lavelinge');
var Lavelinge = mongoose.model('lavelinge', lavelinge);

module.exports = { Lavelinge };