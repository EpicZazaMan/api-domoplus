const {mongoose} = require('../db/connectdb');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const radateur = new mongoose.Schema({
    ID_machine: {
        type: Number,
        default: 5
    },
    temperature: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conso:{
        type: Number,
        required: false
    }
});


radateur.plugin(autoIncrement.plugin, 'radateur');
var Radiateur = mongoose.model('radateur', radateur);

module.exports = { Radiateur };