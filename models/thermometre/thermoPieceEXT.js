const {mongoose} = require('../../db/connectdb');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const thermoEXT = new mongoose.Schema({
    ID_machine: {
        type: Number,
        default: 8
    },
    temperature: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conso:{
        type: Number,
        required: false
    }
});


thermoEXT.plugin(autoIncrement.plugin, 'ThermoEXT');
var ThermoEXT = mongoose.model('ThermoEXT', thermoEXT);

module.exports = { ThermoEXT };
