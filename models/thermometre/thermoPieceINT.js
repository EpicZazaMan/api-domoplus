const {mongoose} = require('../../db/connectdb');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const thermoINT = new mongoose.Schema({
    ID_machine: {
        type: Number,
        default: 9
    },
    temperature: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conso:{
        type: Number,
        required: false
    }
});


thermoINT.plugin(autoIncrement.plugin, 'ThermoINT');
var ThermoINT = mongoose.model('ThermoINT', thermoINT);

module.exports = { ThermoINT };
