const {mongoose} = require('../../db/connectdb');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const lumiere3 = new mongoose.Schema({
    ID_machine: {
        type: Number,
        default: 3
    },
    tempsactif: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conso:{
        type: Number,
        required: false
    }
});


lumiere3.plugin(autoIncrement.plugin, 'lumiere3');
var Lumiere3 = mongoose.model('lumiere3', lumiere3);

module.exports = { Lumiere3 };