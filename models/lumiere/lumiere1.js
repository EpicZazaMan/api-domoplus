const {mongoose} = require('../../db/connectdb');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const lumiere1 = new mongoose.Schema({
    ID_machine: {
        type: Number,
        default: 1
    },
    tempsactif: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conso:{
        type: Number,
        required: false
    }
});


lumiere1.plugin(autoIncrement.plugin, 'lumiere1');
var Lumiere1 = mongoose.model('lumiere1', lumiere1);

module.exports = { Lumiere1 };