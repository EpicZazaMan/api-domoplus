const {mongoose} = require('../../db/connectdb');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const lumiere2 = new mongoose.Schema({
    ID_machine: {
        type: Number,
        default: 2
    },
    tempsactif: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conso:{
        type: Number,
        required: false
    }
});


lumiere2.plugin(autoIncrement.plugin, 'lumiere2');
var Lumiere2 = mongoose.model('lumiere2', lumiere2);

module.exports = { Lumiere2 };