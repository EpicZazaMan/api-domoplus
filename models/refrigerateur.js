const {mongoose} = require('../db/connectdb');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose);


const refrigerateur = new mongoose.Schema({
    ID_machine: {
        type: Number,
        default: 4
    },
    temperature: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    conso:{
        type: Number,
        required: false
    }
});


refrigerateur.plugin(autoIncrement.plugin, 'refrigerateur');
var Refrigerateur = mongoose.model('refrigerateur', refrigerateur);

module.exports = { Refrigerateur };