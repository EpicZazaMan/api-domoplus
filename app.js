const bodyParser = require('body-parser');
const express = require('express');
const app = express();

// require fonction--------------------------------------------------------------------------------------------
const {getThermoEXT,getThermoEXTById,getLastThermoEXT} = require('./fonction/thermo/thermoEXT/get');
const {postThermoEXT} = require('./fonction/thermo/thermoEXT/post');
const {deleteThermoEXTById} = require('./fonction/thermo/thermoEXT/delete');

const {getThermoINT,getThermoINTById,getLastThermoINT} = require('./fonction/thermo/thermoINT/get');
const {postThermoINT} = require('./fonction/thermo/thermoINT/post');
const {deleteThermoINTById} = require('./fonction/thermo/thermoINT/delete');

const {getDomotique,getDomotiqueById,} = require('./fonction/domotique/get');
const {postDomotique} = require('./fonction/domotique/post');
const {deleteDomotiqueById} = require('./fonction/domotique/delete');
const {updateDomotiqueById} = require('./fonction/domotique/put');

const {deleteLavelingeById} = require('./fonction/lavelinge/delete');
const {getLavelinge,getLavelingeById,getLastLavelinge} = require('./fonction/lavelinge/get');
const {postLavelinge} = require('./fonction/lavelinge/post');

const {deleteRadiateurById} = require('./fonction/radiateur/delete');
const {getRadiateur,getRadiateurById,getLastRadiateur} = require('./fonction/radiateur/get');
const {postRadiateur} = require('./fonction/radiateur/post');

const {deleteRefrigerateurById} = require('./fonction/refrigerateur/delete');
const {getRefrigerateur,getRefrigerateurById,getLastRefigerateur} = require('./fonction/refrigerateur/get');
const {postRefrigerateur} = require('./fonction/refrigerateur/post');

const {deleteStoreById} = require('./fonction/store/delete');
const {getStore,getStoreById,getLastStore} = require('./fonction/store/get');
const {postStore} = require('./fonction/store/post');

const {deleteLumiere1ById} = require('./fonction/lumiere/lumiere1/delete');
const {getLumiere1,getLumiere1ById,getLastLumiere1} = require('./fonction/lumiere/lumiere1/get');
const {postLumiere1} = require('./fonction/lumiere/lumiere1/post');

const {deleteLumiere2ById} = require('./fonction/lumiere/lumiere2/delete');
const {getLumiere2,getLumiere2ById,getLastLumiere2} = require('./fonction/lumiere/lumiere2/get');
const {postLumiere2} = require('./fonction/lumiere/lumiere2/post');

const {deleteLumiere3ById} = require('./fonction/lumiere/lumiere3/delete');
const {getLumiere3,getLumiere3ById,getLastLumiere3} = require('./fonction/lumiere/lumiere3/get');
const {postLumiere3} = require('./fonction/lumiere/lumiere3/post');
//Routing-------------------------------------------------------------------------------------------------------
// app.use(allowCrossDomain);
app.use('/', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
   });
app.use(bodyParser.json());
console.log('Nous disposons votre serveur sur le port:3000');
const port = process.env.PORT || 3000
app.listen(port);

// routes ----------------------------------------------------------------------------

app.get("/domotique",(req,res, next)=>{
    getDomotique(req,res);
});
app.get("/domotique/:id",(req,res, next)=>{
    getDomotiqueById(req,res);
});
app.post("/domotique",(req,res, next)=>{
    postDomotique(req,res);
});;
app.delete("/domotique/:id",(req,res, next)=>{
    deleteDomotiqueById(req,res);
});
app.put("/domotique/:id",(req,res, next)=>{
    updateDomotiqueById(req,res);
});


// Appareils--------------------------------------------------------------------------------
app.get("/thermo/EXT",(req,res, next)=>{
    getThermoEXT(req,res);
});
app.get("/thermo/EXT/:id",(req,res, next)=>{
    getThermoEXTById(req,res);
});
app.get("/thermo/EXTlast",(req,res, next)=>{
    getLastThermoEXT(req,res);
});
app.post("/thermo/EXT",(req,res, next)=>{
    postThermoEXT(req,res);
});
app.delete("/thermo/EXT/:id",(req,res, next)=>{
    deleteThermoEXTById(req,res);
});

app.get("/thermo/INT",(req,res, next)=>{
    getThermoINT(req,res);
});
app.get("/thermo/INT/:id",(req,res, next)=>{
    getThermoINTById(req,res);
});
app.get("/thermo/INTlast",(req,res, next)=>{
    getLastThermoINT(req,res);
});
app.post("/thermo/INT",(req,res, next)=>{
    postThermoINT(req,res);
});
app.delete("/thermo/INT/:id",(req,res, next)=>{
    deleteThermoINTById(req,res);
});

app.get("/lavelinge",(req,res, next)=>{
    getLavelinge(req,res);
});
app.get("/lavelinge/:id",(req,res, next)=>{
    getLavelingeById(req,res);
});
app.get("/lavelingelast",(req,res, next)=>{
    getLastLavelinge(req,res);
});
app.post("/lavelinge",(req,res, next)=>{
    postLavelinge(req,res);
});
app.delete("/lavelinge/:id",(req,res, next)=>{
    deleteLavelingeById(req,res);
});

app.get("/refrigerateur",(req,res, next)=>{
    getRefrigerateur(req,res);
});
app.get("/refrigerateur/:id",(req,res, next)=>{
    getRefrigerateurById(req,res);
});
app.get("/refrigerateurlast",(req,res, next)=>{
    getLastRefigerateur(req,res);
});
app.post("/refrigerateur",(req,res, next)=>{
    postRefrigerateur(req,res);
});
app.delete("/refrigerateur/:id",(req,res, next)=>{
    deleteRefrigerateurById(req,res);
});

app.get("/store",(req,res, next)=>{
    getStore(req,res);
});
app.get("/store/:id",(req,res, next)=>{
    getStoreById(req,res);
});
app.get("/storelast",(req,res, next)=>{
    getLastStore(req,res);
});
app.post("/store",(req,res, next)=>{
    postStore(req,res);
});
app.delete("/store/:id",(req,res, next)=>{
    deleteStoreById(req,res);
});

app.get("/radiateur",(req,res, next)=>{
    getRadiateur(req,res);
});
app.get("/radiateur/:id",(req,res, next)=>{
    getRadiateurById(req,res);
});
app.get("/radiateurlast",(req,res, next)=>{
    getLastRadiateur(req,res);
});
app.post("/radiateur",(req,res, next)=>{
    postRadiateur(req,res);
});
app.delete("/radiateur/:id",(req,res, next)=>{
    deleteRadiateurById(req,res);
});

app.get("/lumiere1",(req,res, next)=>{
    getLumiere1(req,res);
});
app.get("/lumiere1/:id",(req,res, next)=>{
    getLumiere1ById(req,res);
});
app.get("/lumiere1last",(req,res, next)=>{
    getLastLumiere1(req,res);
});
app.post("/lumiere1",(req,res, next)=>{
    postLumiere1(req,res);
});
app.delete("/lumiere1/:id",(req,res, next)=>{
    deleteLumiere1ById(req,res);
});
app.get("/lumiere2",(req,res, next)=>{
    getLumiere2(req,res);
});
app.get("/lumiere2/:id",(req,res, next)=>{
    getLumiere2ById(req,res);
});
app.get("/lumiere2last",(req,res, next)=>{
    getLastLumiere2(req,res);
});
app.post("/lumiere2",(req,res, next)=>{
    postLumiere2(req,res);
});
app.delete("/lumiere2/:id",(req,res, next)=>{
    deleteLumiere2ById(req,res);
});
app.get("/lumiere3",(req,res, next)=>{
    getLumiere3(req,res);
});
app.get("/lumiere3/:id",(req,res, next)=>{
    getLumiere3ById(req,res);
});
app.get("/lumiere3last",(req,res, next)=>{
    getLastLumiere3(req,res);
});
app.post("/lumiere3",(req,res, next)=>{
    postLumiere3(req,res);
});
app.delete("/lumiere3/:id",(req,res, next)=>{
    deleteLumiere3ById(req,res);
});