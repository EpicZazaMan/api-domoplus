const { Store } = require('../../models/store');

const postStore = (req,res) => {
    const store = new Store({
        ouverture: req.body.ouverture,
        conso: req.body.conso
    });
    store.save().then(store => {
        res.send(store);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postStore};

