const { Store } = require('../../models/store');

const getStore = (req,res) => {
    Store.find().then((store)=>{
        res.send(store);
    },e=>res.status(500).send(e))
};

const getStoreById = (req,res) => {
    const {id} = req.params;

    Store.findById(id).then((store)=>{
        if (!store) {
            res.status(404).send();
        }
        res.send(store);
    },e=>res.status(500).send(e))
};
const getLastStore = (req,res) => {
    Store.findOne().sort({ date: -1 }).limit(1).then((store)=>{
        res.send(store);
    },e=>res.status(500).send(e))
};

module.exports={getStore,getStoreById,getLastStore};