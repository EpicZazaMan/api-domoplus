const { Store } = require('../../models/store');

const deleteStoreById = (req,res) => {
    const {id} = req.params;

    Store.remove({"_id": id}).then((store)=>{
        if (!store) {
            res.status(404).send();
        }
        res.send(store);
    },e=>res.status(500).send(e));
};

module.exports={deleteStoreById};