const { Radiateur } = require('../../models/radiateur');

const postRadiateur = (req,res) => {
    const radiateur = new Radiateur({
        temperature: req.body.temperature,
        conso: req.body.conso
    });
    radiateur.save().then(radiateur => {
        res.send(radiateur);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postRadiateur};

