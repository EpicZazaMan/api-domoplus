const { Radiateur } = require('../../models/radiateur');

const deleteRadiateurById = (req,res) => {
    const {id} = req.params;

    Radiateur.remove({"_id": id}).then((radiateur)=>{
        if (!radiateur) {
            res.status(404).send();
        }
        res.send(radiateur);
    },e=>res.status(500).send(e));
};

module.exports={deleteRadiateurById};