const { Radiateur } = require('../../models/radiateur');

const getRadiateur = (req,res) => {
    Radiateur.find().then((radiateur)=>{
        res.send(radiateur);
    },e=>res.status(500).send(e))
};

const getRadiateurById = (req,res) => {
    const {id} = req.params;

    Radiateur.findById(id).then((radiateur)=>{
        if (!radiateur) {
            res.status(404).send();
        }
        res.send(radiateur);
    },e=>res.status(500).send(e))
};

const getLastRadiateur = (req,res) => {
    Radiateur.findOne().sort({ date: -1 }).limit(1).then((radiateur)=>{
        res.send(radiateur);
    },e=>res.status(500).send(e))
};

module.exports={getRadiateur,getRadiateurById,getLastRadiateur};