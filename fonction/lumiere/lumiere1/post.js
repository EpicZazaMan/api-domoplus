const { Lumiere1 } = require('../../../models/lumiere/lumiere1');
const postLumiere1 = (req,res) => {
    const lumiere = new Lumiere1({
        conso: req.body.conso,
        tempsactif: req.body.tempsactif
    });
    lumiere.save().then(lumiere => {
        res.send(lumiere);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postLumiere1};

