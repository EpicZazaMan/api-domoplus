const { Lumiere1 } = require('../../../models/lumiere/lumiere1');
const getLumiere1 = (req,res) => {
    Lumiere1.find().then((lumiere)=>{
        res.send(lumiere);
    },e=>res.status(500).send(e))
};

const getLumiere1ById = (req,res) => {
    const {id} = req.params;

    Lumiere1.findById(id).then((lumiere)=>{
        if (!lumiere) {
            res.status(404).send();
        }
        res.send(lumiere);
    },e=>res.status(500).send(e))
};
const getLastLumiere1 = (req,res) => {
    Lumiere1.findOne().sort({ date: -1 }).limit(1).then((lumiere)=>{
        res.send(lumiere);
    },e=>res.status(500).send(e))
};


module.exports={getLumiere1,getLumiere1ById,getLastLumiere1};