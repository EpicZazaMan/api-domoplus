const { Lumiere1 } = require('../../../models/lumiere/lumiere1');
const deleteLumiere1ById = (req,res) => {
    const {id} = req.params;

    Lumiere1.remove({"_id": id}).then((lumiere)=>{
        if (!lumiere) {
            res.status(404).send();
        }
        res.send(lumiere);
    },e=>res.status(500).send(e));
};

module.exports={deleteLumiere1ById};