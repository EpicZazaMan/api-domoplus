const { Lumiere2 } = require('../../../models/lumiere/lumiere2');
const deleteLumiere2ById = (req,res) => {
    const {id} = req.params;

    Lumiere2.remove({"_id": id}).then((lumiere)=>{
        if (!lumiere) {
            res.status(404).send();
        }
        res.send(lumiere);
    },e=>res.status(500).send(e));
};

module.exports={deleteLumiere2ById};