const { Lumiere2 } = require('../../../models/lumiere/lumiere2');
const getLumiere2 = (req,res) => {
    Lumiere2.find().then((lumiere)=>{
        res.send(lumiere);
    },e=>res.status(500).send(e))
};

const getLumiere2ById = (req,res) => {
    const {id} = req.params;

    Lumiere2.findById(id).then((lumiere)=>{
        if (!lumiere) {
            res.status(404).send();
        }
        res.send(lumiere);
    },e=>res.status(500).send(e))
};
const getLastLumiere2= (req,res) => {
    Lumiere2.findOne().sort({ date: -1 }).limit(1).then((lumiere)=>{
        res.send(lumiere);
    },e=>res.status(500).send(e))
};

module.exports={getLumiere2,getLumiere2ById,getLastLumiere2};