const { Lumiere2 } = require('../../../models/lumiere/lumiere2');
const postLumiere2 = (req,res) => {
    const lumiere = new Lumiere2({
        conso: req.body.conso,
        tempsactif: req.body.tempsactif
    });
    lumiere.save().then(lumiere => {
        res.send(lumiere);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postLumiere2};

