const { Lumiere3 } = require('../../../models/lumiere/lumiere3');
const postLumiere = (req,res) => {
    const lumiere = new Lumiere({
        conso: req.body.conso,
        tempsactif: req.body.tempsactif
    });
    lumiere.save().then(lumiere => {
        res.send(lumiere);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postLumiere};

