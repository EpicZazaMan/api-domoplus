const { Lumiere3 } = require('../../../models/lumiere/lumiere3');
const getLumiere3 = (req,res) => {
    Lumiere3.find().then((lumiere)=>{
        res.send(lumiere);
    },e=>res.status(500).send(e))
};

const getLumiere3ById = (req,res) => {
    const {id} = req.params;

    Lumiere3.findById(id).then((lumiere)=>{
        if (!lumiere) {
            res.status(404).send();
        }
        res.send(lumiere);
    },e=>res.status(500).send(e))
};
const getLastLumiere3= (req,res) => {
    Lumiere3.findOne().sort({ date: -1 }).limit(1).then((lumiere)=>{
        res.send(lumiere);
    },e=>res.status(500).send(e))
};

module.exports={getLumiere3,getLumiere3ById,getLastLumiere3};