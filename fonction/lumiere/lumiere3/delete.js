const { Lumiere3 } = require('../../../models/lumiere/lumiere3');
const deleteLumiereById = (req,res) => {
    const {id} = req.params;

    Lumiere.remove({"_id": id}).then((lumiere)=>{
        if (!lumiere) {
            res.status(404).send();
        }
        res.send(lumiere);
    },e=>res.status(500).send(e));
};

module.exports={deleteLumiereById};