const { ThermoEXT } = require('../../../models/thermometre/thermoPieceEXT');

const postThermoEXT = (req,res) => {
    const thermoEXT = new ThermoEXT({
        conso: req.body.conso,
        temperature: req.body.conso
    });
    thermoEXT.save().then(thermoEXT => {
        res.send(thermoEXT);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postThermoEXT};

