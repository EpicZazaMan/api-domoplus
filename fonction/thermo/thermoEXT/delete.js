const { ThermoEXT } = require('../../../models/thermometre/thermoPieceEXT');


const deleteThermoEXTById = (req,res) => {
    const {id} = req.params;

    ThermoEXT.remove({"_id": id}).then((thermoEXT)=>{
        if (!thermoEXT) {
            res.status(404).send();
        }
        res.send(thermoEXT);
    },e=>res.status(500).send(e));
};

module.exports={deleteThermoEXTById};