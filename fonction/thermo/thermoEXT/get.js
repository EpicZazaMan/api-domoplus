const { ThermoEXT } = require('../../../models/thermometre/thermoPieceEXT');

const getThermoEXT = (req,res) => {
    ThermoEXT.find().then((thermoEXT)=>{
        res.send(thermoEXT);
    },e=>res.status(500).send(e))
};

const getThermoEXTById = (req,res) => {
    const {id} = req.params;

    ThermoEXT.findById(id).then((thermoEXT)=>{
        if (!thermoEXT) {
            res.status(404).send();
        }
        res.send(thermoEXT);
    },e=>res.status(500).send(e))
};

const getLastThermoEXT = (req,res) => {
    ThermoEXT.findOne().sort({ date: -1 }).limit(1).then((thermoEXT)=>{
        res.send(thermoEXT);
    },e=>res.status(500).send(e))
};


module.exports={getThermoEXT,getThermoEXTById,getLastThermoEXT};