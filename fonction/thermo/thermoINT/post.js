const { ThermoINT } = require('../../../models/thermometre/thermoPieceINT');

const postThermoINT = (req,res) => {
    const thermoINT = new ThermoINT({
        conso: req.body.conso,
        temperature: req.body.conso
    });
    thermoINT.save().then(thermoINT => {
        res.send(thermoINT);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postThermoINT};

