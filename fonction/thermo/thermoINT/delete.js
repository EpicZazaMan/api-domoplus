const { ThermoINT } = require('../../../models/thermometre/thermoPieceINT');


const deleteThermoINTById = (req,res) => {
    const {id} = req.params;

    ThermoINT.remove({"_id": id}).then((thermoINT)=>{
        if (!thermoINT) {
            res.status(404).send();
        }
        res.send(thermoINT);
    },e=>res.status(500).send(e));
};

module.exports={deleteThermoINTById};