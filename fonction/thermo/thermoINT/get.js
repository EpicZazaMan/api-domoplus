const { ThermoINT } = require('../../../models/thermometre/thermoPieceINT');

const getThermoINT = (req,res) => {
    ThermoINT.find().then((thermoINT)=>{
        res.send(thermoINT);
    },e=>res.status(500).send(e))
};

const getThermoINTById = (req,res) => {
    const {id} = req.params;

    ThermoINT.findById(id).then((thermoINT)=>{
        if (!thermoINT) {
            res.status(404).send();
        }
        res.send(thermoINT);
    },e=>res.status(500).send(e))
};

const getLastThermoINT = (req,res) => {
    ThermoINT.findOne().sort({ date: -1 }).limit(1).then((thermoINT)=>{
        res.send(thermoINT);
    },e=>res.status(500).send(e))
};


module.exports={getThermoINT,getThermoINTById,getLastThermoINT};