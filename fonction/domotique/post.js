const { Domotique } = require('../../models/domotique');

const postDomotique = (req,res) => {
    const domotique = new Domotique({
        nom: req.body.nom,
        icon: req.body.icon
    });
    domotique.save().then(domotique => {
        res.send(domotique);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postDomotique};

