const { Domotique } = require('../../models/domotique');

const deleteDomotiqueById = (req,res) => {
    const {id} = req.params;

    Domotique.remove({"_id": id}).then((domotique)=>{
        if (!domotique) {
            res.status(404).send();
        }
        res.send(domotique);
    },e=>res.status(500).send(e));
};

module.exports={deleteDomotiqueById};