const { Domotique } = require('../../models/domotique');

const updateDomotiqueById = (req,res) => {
    const {id} = req.params;
    const domotique = new Domotique({
        etat: req.body.etat
    });
    Domotique.findByIdAndUpdate(id,domotique).then((domotique)=>{
        res.send(domotique);
    },e=>res.status(500).send(e))
};

module.exports={updateDomotiqueById};