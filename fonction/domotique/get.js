const { Domotique } = require('../../models/domotique');

const getDomotique = (req,res) => {
    Domotique.find().then((domotique)=>{
        res.send(domotique);
    },e=>res.status(500).send(e))
};

const getDomotiqueById = (req,res) => {
    const {id} = req.params;

    Domotique.findById(id).then((domotique)=>{
        if (!domotique) {
            res.status(404).send();
        }
        res.send(domotique);
    },e=>res.status(500).send(e))
};

module.exports={getDomotique,getDomotiqueById};