const { Refrigerateur } = require('../../models/refrigerateur');

const getRefrigerateur = (req,res) => {
    Refrigerateur.find().then((refrigerateur)=>{
        res.send(refrigerateur);
    },e=>res.status(500).send(e))
};

const getRefrigerateurById = (req,res) => {
    const {id} = req.params;

    Refrigerateur.findById(id).then((refrigerateur)=>{
        if (!refrigerateur) {
            res.status(404).send();
        }
        res.send(refrigerateur);
    },e=>res.status(500).send(e))
};

const getLastRefigerateur = (req,res) => {
    Refrigerateur.findOne().sort({ date: -1 }).limit(1).then((refrigerateur)=>{
        res.send(refrigerateur);
    },e=>res.status(500).send(e))
};

module.exports={getRefrigerateur,getRefrigerateurById,getLastRefigerateur};