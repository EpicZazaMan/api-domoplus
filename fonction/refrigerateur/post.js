const { Refrigerateur } = require('../../models/refrigerateur');

const postRefrigerateur = (req,res) => {
    const refrigerateur = new Refrigerateur({
        conso: req.body.conso,
        temperature: req.body.temperature
    });
    refrigerateur.save().then(refrigerateur => {
        res.send(refrigerateur);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postRefrigerateur};

