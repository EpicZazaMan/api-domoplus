const { Refrigerateur } = require('../../models/refrigerateur');

const deleteRefrigerateurById = (req,res) => {
    const {id} = req.params;

    Refrigerateur.remove({"_id": id}).then((refrigerateur)=>{
        if (!refrigerateur) {
            res.status(404).send();
        }
        res.send(refrigerateur);
    },e=>res.status(500).send(e));
};

module.exports={deleteRefrigerateurById};