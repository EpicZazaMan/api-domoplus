const { Lavelinge } = require('../../models/lavelinge');

const getLavelinge = (req,res) => {
    Lavelinge.find().then((lavelinge)=>{
        res.send(lavelinge);
    },e=>res.status(500).send(e))
};

const getLavelingeById = (req,res) => {
    const {id} = req.params;

    Lavelinge.findById(id).then((lavelinge)=>{
        if (!lavelinge) {
            res.status(404).send();
        }
        res.send(lavelinge);
    },e=>res.status(500).send(e))
};
const getLastLavelinge = (req,res) => {
    Lavelinge.findOne().sort({ date: -1 }).limit(1).then((lumiere)=>{
        res.send(lumiere);
    },e=>res.status(500).send(e))
};

module.exports={getLavelinge,getLavelingeById,getLastLavelinge};