const { Lavelinge } = require('../../models/lavelinge');

const postLavelinge = (req,res) => {
    const lavelinge = new Lavelinge({
        conso: req.body.conso,
        lavage: req.body.lavage
    });
    lavelinge.save().then(lavelinge => {
        res.send(lavelinge);
    }, e => {
        res.status(500).send(e);
    });
};


module.exports={postLavelinge};

