const { Lavelinge } = require('../../models/lavelinge');

const deleteLavelingeById = (req,res) => {
    const {id} = req.params;

    Lavelinge.remove({"_id": id}).then((lavelinge)=>{
        if (!lavelinge) {
            res.status(404).send();
        }
        res.send(lavelinge);
    },e=>res.status(500).send(e));
};

module.exports={deleteLavelingeById};